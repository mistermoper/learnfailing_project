#!/usr/bin/env bash
# @NOTE: NEVER use for production!
mkdir -p web/sites/default/files/behat/errors
mkdir -p web/sites/default/files/behat/screenshots
mkdir -p web/sites/default/files/behat/pages
mkdir -p reports
docker-compose exec -u root php sh -c 'chown wodby:www-data reports -R'
docker-compose exec -u root php sh -c 'chown wodby:www-data web/sites/default/files -R'
docker-compose exec -u root php sh -c 'chmod ug+rw reports -R'
docker-compose exec -u root php chown wodby:www-data web/sites/default/settings.local.php
