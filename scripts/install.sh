#!/usr/bin/env bash
cp .env.example .env
docker-compose up -d
sleep 15
./scripts/install-dependencies.sh
cp web/sites/default/example.settings.local.php web/sites/default/settings.local.php
./scripts/setup-folders.sh
docker-compose exec php sh -c "drush si -y --site-name='Learn failing!'"
make drush cr
