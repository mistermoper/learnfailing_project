#!/usr/bin/env bash
docker-compose exec -T php sh -c "composer install"
cd web/profiles/contrib/learnfailing && git submodule update --init --recursive
