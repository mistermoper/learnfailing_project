#!/usr/bin/env bash
echo "docker-compose exec -u www-data php sh -c './vendor/bin/behat --config tests/behat/behat.yml $@'"
docker-compose exec -u www-data -T php ./vendor/bin/behat --colors --config tests/behat/behat.yml $@
